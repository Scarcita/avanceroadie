import React, { useEffect, useState } from 'react';
import Image from 'next/image';
import { Spinner, Dots } from 'react-activity';
import "react-activity/dist/Spinner.css";
import "react-activity/dist/Dots.css";


export default function ModalDetalle(props) {
  const [showModal, setShowModal] = useState(false);

    return (
        <>
            <div>
                <div
                    onClick={() => setShowModal(true)}
                >
                    Create post
                </div>
            </div>
            {showModal ? (
                <>
                    <div className="fixed inset-0 z-10 overflow-y-auto">
                        <div
                            className="fixed inset-0 w-full h-full bg-[#000000] opacity-40"
                            onClick={() => setShowModal(false)}
                        ></div>
                        <div className="flex items-center min-h-screen px-4 py-8">
                            <div className="relative w-full max-w-[600px] p-4 mx-auto bg-[#FFFF] rounded-md shadow-lg">

                                <div>
                                    <h4 className="text-lg font-medium text-gray-800">
                                        CREATE POST
                                    </h4>

                                </div>

                              
                            </div>
                        </div>
                    </div>
                </>
            ) : null}
        </>
    );
}