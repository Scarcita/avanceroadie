import React from 'react';
import Spinner from 'react-activity/dist/Spinner';
import 'react-activity/dist/Spinner.css';
import Image from 'next/image';
import { useEffect, useState } from 'react'


 const AllPosts = () => {

    const [isLoading, setIsLoading] = useState(true)

    const [showDots, setShowDots] = useState(true);
    const [total, setTotal] = useState(0);
    const [data, setData] = useState([]);
    
    useEffect(() => {
    fetch('https://slogan.com.bo/roadie/clientsPlansPosts/monthCalendar')
    .then(response => response.json())
    .then(data => {
        if (data.status) {
            console.log('ENDPOINT CALENDAR:', Object.values(data.data));
            setData(data.data)

            //console.log(data)

        } else {
            console.error(data.error)
        }
        setIsLoading(false)
    })
  }, [])
  

    return (
        isLoading ?
            <>
                <div className='flex justify-center items-center' style={{ width: '100%', height: 70 }}>
                    <Spinner color="#582BE7" size={17} speed={1} animating={true} style={{ marginLeft: 'auto', marginRight: 'auto' }} />
                </div>

            </>
            :
            <>
                <div>
                    <CardPost data={data}/>
                </div>
            </>
    )
}


const CardPost = (props) => {
    const { data } = props;

    if(data !== undefined){
      if (data !== null){

        return (
          data !== undefined ?
            <div className='h-[100px] overflow-y-auto rounded-lg bg-gradient-to-r from-indigo-500 via-purple-500 to-pink-500 ml-[10px] pl-[3px]'>
              
              {
                data.map(post => {
    
                  
    
                  if(post !== undefined){
                      if (post !== null){
                        if(post.type !== null && post.type !== undefined){
                          switch (post.type) {
                            case 'Image': 
                              imgType = '/Board/image.png'         
                                break;
                            case 'Album':
                              imgType= '/Board/image.png'
                                break;
                            case 'Video':
                              imgType = '/Board/video.png'
                            
                            break;
                            case 'story':
                              imgType = '/Board/video.png'
                            
                            break;
                            case 'Reel':
                              imgType = '/Board/video.png'
                            
                            break;
                                
                            default:
                                break;
                        }
                        }
                      }
                  }
                  const imgType = "";
                  
                  return (
                    
                    <div className='grid grid-cols-12 gap-3'
                    key={post.id}>
                      <div className='col-span-7 md:col-span-7 lg:col-span-7'>
                        <div className='grid grid-cols-12 gap-2 mt-[5px] mb-[5px]'>
                          <div className='col-span-12 md:col-span-12 lg:col-span-12'>
                            {
                            (post.media_url != null && post.media_url.length > 0)
                            ?
                            (
                            <div className='mt-[10px]'>
                              <Image
                                className='rounded-full'
                                src={post.media_url}
                                alt='media'
                                height={37}
                                width={37}>
                              </Image>
    
                              {post.clients_plan.client != null && post.clients_plan.client.img_url.length > 0 ?
                              
                                <Image 
                                  className='rounded-full'
                                  src={post.clients_plan.client.img_url}
                                  alt='media'
                                  height={17}
                                  width={17}>
    
                                </Image> 
                                : <></>  
                              } 
                              
                            
                            </div>
                          ) : <></>
                          }
    
                          <div className='text-[10px] text-left font-semibold leading-2 whitespace-normal'>7:00 AM</div>
    
                          {post.title !== null ?
                              <p className='text-[10px] font-semibold text-left leading-2 whitespace-normal text-ellipsis ...'>
                                {post.title}
                              </p>
                              : <></>
                            }
                            {post.subtitle !== null && post.subtitle !== undefined ?
    
                            <div className='mt-[2px] mb-[2px]'>
                              <p className='text-[8px] text-[#000000] text-left font-light leading-2 whitespace-normal text-ellipsis ... '>{post.subtitle}</p>
                            </div>
                            : <></>
                            }
                            {post.instructions !== null && post.instructions !== undefined ?
                            <div className='mt-[2px] mb-[2px]'>
                              <p className='font-semibold text-[12px] text-left text-[#582BE7]'>Instructions: </p>
                              <p className='text-[8px] text-left leading-2 whitespace-normal text-ellipsis ... '>{post.instructions}</p>
                            </div>
                            : <></>
                            }
    
                            {post.post_copy !== null && post.post_copy !== undefined ?
                            <div className='mt-[2px] mb-[2px]'>
                              <p className='font-semibold text-[12px] text-left text-[#582BE7]'>Post copy: </p>
                              <p className='text-[8px] text-left leading-2 whitespace-normal text-ellipsis ... '>{post.post_copy}</p>
                            </div>
                            : <></>
                            }
    
                          </div>
                        </div>
    
                        <div className='col-span-5 md:col-span-5 lg:col-span-5'>
                          <div className='grid grid-cols-12'>
                            <div className='col-span-12 md:col-span-12 lg:col-span-12'>
                              {/* {item.type !== null ? */}
                                <div className=''>
                                  <p className='font-semibold text-left text-[12px] text-[#582BE7]'>Type: </p>
                                  <Image
                                    className='text-center items-center'
                                    src={imgType}
                                    alt='img'
                                    layout='fixed'
                                    width={30}
                                    height={30}
                                  />
                                </div>
                              {/* : <></>
                              } */}
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>             
                  )
                      
                })
        
              }
    
            </div>
            : <></>
        );

      }
    }

    
};
export default AllPosts;