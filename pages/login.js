import React, {useEffect, useState} from 'react'
import { useForm } from 'react-hook-form'
import { yupResolver } from '@hookform/resolvers/yup'
import * as Yup from 'yup'

import Link from 'next/link'
import { useRouter } from 'next/router'
import { signIn} from 'next-auth/react'

import { Dots } from 'react-activity'
import "react-activity/dist/Dots.css"


export default function Login() {

  const router = useRouter()

  const [authState, setAuthState] = useState({
    username: '',
    password: '',
    role: 'ADMIN',
    name: ''
  })
  const [pageState, setPageState] = useState({
    error: '',
    processing: false
  })

  const handleFieldChange = (e) => {
    setAuthState(old => ({ ...old, [e.target.id]: e.target.value}))
  }

  useEffect(() => {
    console.warn(authState)
  }, [authState])

  const handleAuth = async () => {
    setPageState(old => ({...old, processing: true, error: ''}))

    const result = await sendUserData();

    console.warn('Result: ' + result)
    console.warn('authState: ' + authState)
    console.warn(authState)

    if(result) {
      console.warn('true!!')
      signIn('credentials', {
        ...authState,
        redirect: false
      }).then(response => {
        //console.warn('response from auth' + response)
        //alert(JSON.stringify(response, null, 4))
        if (response.ok) {
          router.push("/")
          router.push(response.url)
        }else {
          setPageState(old => ({ ...old, processing: false, error: response.error}))
        }
      }).catch(error => {
        //console.log(error)
        setPageState(old => ({...old, processing: false, error: error.message ?? "Something went wrong!"}))
      })
    } else {
      setPageState(old => ({...old, processing: false, error: "Something went wrong!"}))
    }
    
  }

 
  //////////////// VALIDATE USERS ////////////////



  const sendUserData = async () => {

    var data = new FormData();
    data.append("username", authState.username);
    data.append("password", authState.password);

    const response = await fetch("https://slogan.com.bo/vulcano/users/loginMobile", {
      method: 'POST',
      body: data,
    })
      .then(response => response.json())
      .then(data => {
        //console.log('loginMobile response: ', data);
        if(data.status){
          console.warn(data.data);
          console.warn(data.data.role);
          console.warn(data.data.id);
          console.warn(data.data.name);         
          setAuthState(old => ({ ...old, id: data.data.id}))
          setAuthState(old => ({ ...old, name: data.data.name}))
          setAuthState(old => ({ ...old, role: data.data.role})) 
          return true;
        } else {
          alert('Nombre de usuario y/o incorrecto(s)')
          //alert(JSON.stringify(data.errors, null, 4))
          return false
        }
      });

    return response;
  }

  ///////////////// VALIDATIONS INPUT ///////////////
  
    const validationSchema = Yup.object().shape({
        username: Yup.string()
        .required('Username is required'),
        password: Yup.string()
        // .min(3, "Passaword must be at least 3 characters")
        .required("Password is required")
    })

    const formOptions = { resolver: yupResolver(validationSchema)};
    const {register, handleSubmit, reset, formState } = useForm(formOptions);
    const { errors } = formState;

    function onSubmit(data) {
        //alert('SUCCESS!! :-)\n\n' + JSON.stringify(data, null, 4));
        handleAuth();
        return false;
    }


  return (
     
    <div>

      <div className="grid grid-cols-12 mt-[20px] ml-[20px] mr-[20px]">

        <form onSubmit={handleSubmit(onSubmit)}>
          <div className="w-full h-full flex flex-col justify-center self-center">
            <div className="col-span-12 md:col-span-6 lg:col-span-6 form-group ">

              <label htmlFor="first" className="text-[14px] mb-1 text-[#3A4567]">Usuario</label>

              <input id="username" name="username" type='text' className={`form-control ${errors.username ? 'is-invalid' : ''} w-[340px] h-[48px] rounded-[7px] bg-[#F6F6FA] border-[1px] border-[#E4E7EB] mb-3 px-3`}
                  {...register('username')} 
                  value={authState.username}
                  onChange={handleFieldChange}
                  />

              <div className="invalid-feedback">{errors.username?.message}</div>
            </div>

            <div className="col-span-12 md:col-span-6 lg:col-span-6 form-group ">

              <label htmlFor="last" className="text-[14px] mb-1 text-[#3A4567]">Password</label>

              <input id="password" name="password" className={`form-control ${errors.password ? 'is-invalid' : ''} w-[340px] h-[48px] rounded-[7px] bg-[#F6F6FA] border-[1px] border-[#E4E7EB] mb-3 px-3`} 
                  {...register('password')}
                  value={authState.password}
                  onChange={handleFieldChange}
                  />

              <div className="invalid-feedback">{errors.password?.message}</div>
              
            </div>

            <button className="w-[340px] h-[48px] rounded-[7px] bg-[#3682F7] border-0 text-[#FFFFFF] text-[14PX]"
            disabled={pageState.processing} 
            type="submit"
            >
            {pageState.processing ? <Dots className='m-auto' size={17} color={'#fff'}></Dots> : 'Ingresar'}
            </button>

          </div>
        </form>

      </div>

    </div>
  )
}


