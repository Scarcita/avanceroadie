import React, { useState, useEffect } from "react";
import Image from 'next/image';
import Tippy from '@tippyjs/react'
import 'tippy.js/dist/tippy.css'
import 'tippy.js/animations/scale.css'
import 'tippy.js/themes/light.css';
import ReactSelect from 'react-select';
import Link from "next/link";

import { useForm } from 'react-hook-form';
import { yupResolver } from '@hookform/resolvers/yup';
import * as Yup from 'yup';

import ImgFacebook from '../../../public/Plans/facebook.svg'
import ImgGmail from '../../../public/Plans/gmail.svg'
import ImgIg from '../../../public/Plans/instagram.svg'
import ImgMessenger from '../../../public/Plans/messenger.svg'
import ImgTelegram from '../../../public/Plans/telegram.svg'
import ImgTiktok from '../../../public/Plans/tikTok.svg'
import ImgTwitter from '../../../public/Plans/twitter.svg'
import ImgWhatsapp from '../../../public/Plans/whatsapp.svg'
import ImgYoutube from '../../../public/Plans/youtube.svg'

export default function TooltipPlans(props) {
    const {client_id} = props;
    const { 
    id, 
    name,
    facebookPostsQty,
    instagram_posts_qty,
    mailing_posts_qty,
    youtube_posts_qty,
    tiktok_posts_qty,
    linkedin_posts_qty,
    twitter_posts_qty,
    has_included_ads,
    included_ads_qty,
    price,
     } = props;
    const [isLoading, setIsLoading] = useState(false)
    const [showDeleteModal, setShowDeleteModal] = useState(false);
    const [showEditModal, setShowEditModal] = useState(false);
    const [nameForm, setNameForm] = useState(name)
    const [facebookForm, setFacebookForm] = useState('')
    const [facebookPostsGtyForm, setFacebookPostsGtyForm] = useState(facebookPostsQty)
    const [instagramForm, setInstagramForm] = useState('')
    const [instagramPostsGtyForm, setInstagramPostsGtyForm] = useState(instagram_posts_qty)
    const [mailingForm, setMailingForm] = useState('')
    const [mailingPostsGtyForm, setMailingPostsGtyForm] = useState(mailing_posts_qty)
    const [youtubeForm, setYoutubeForm] = useState('')
    const [youtubePostsGtyForm, setYoutubePostsGtyForm] = useState(youtube_posts_qty)
    const [tiktokForm, setTiktokForm] = useState('')
    const [tiktokPostsGtyForm, setTiktokPostsGtyForm] = useState(tiktok_posts_qty)
    const [linkedinForm, setLinkedinForm] = useState('')
    const [linkedinPostsGtyForm, setLinkedinPostsGtyForm] = useState(linkedin_posts_qty)
    const [twitterForm, setTwitterForm] = useState('')
    const [twitterPostsGtyForm, setTwitterPostsGtyForm] = useState(twitter_posts_qty)
    const [hasIncludedAdsForm, setHasIncludedAdsForm] = useState(has_included_ads)
    const [includedAdsQtyForm, setIncludedAdsQtyForm] = useState(included_ads_qty)
    const [priceForm, setPriceForm] = useState(price)


    const actualizarDatos = () => {
        let data = new FormData();

        data.append("name", producto);
        data.append("code", codigo);
        data.append("stock_qty", stockMax);
        data.append("min_stock", stockMin);
        data.append("price", precio);
        data.append("avg_cost", costoPromedio);

        fetch('https://slogan.com.bo/vulcano/products/editMobile/' + id, {
            method: "POST",
            body: data,
        })
            .then(response => response.json())
            .then(data => {
                if (data.status) {
                    getDatos();
                    //console.log(data.data);
                } else {
                    console.error(data.error)
                }
            })
    }

    const eliminarDatos = ( ) => {
        let data = new FormData();

        data.append("name", producto);
        data.append("code", codigo);
        data.append("stock_qty", stockMax);
        data.append("min_stock", stockMin);
        data.append("price", precio);
        data.append("avg_cost", costoPromedio);

        fetch('https://slogan.com.bo/vulcano/products/deleteMobile/' + id, {
            method: "POST",
            body: data,
        })
            .then(response => response.json())
            .then(data => {
                if (data.status) {
                    getDatos();
                    //console.log(data.data);
                } else {
                    console.error(data.error)
                }
            })
    }

    const validationSchema = Yup.object().shape({
        codigo: Yup.string()
            .required('Is required')
            .matches(/^[a-zA-Z0-9-]*$/, 'Ingreso números y letras'),
        producto: Yup.string()
            .required('Is required')
            .matches(/^[a-zA-Z0-9-]*$/, 'Ingreso números y letras'),
        precio: Yup.string()
            .required('Is required')
            .matches(/^[0-9]*$/, 'Ingrese un numero valido'),
        costoPromedio: Yup.string()
            .required('Is required')
            .matches(/^[0-9]*$/, 'Ingrese un numero valido'),
        stockMin: Yup.string()
            .required('Is required')
            .matches(/^[0-9]*$/, 'Ingrese un numero valido'),

        stockMax: Yup.string()
            .required('Is required')
            .matches(/^[0-9]*$/, 'Ingrese un numero valido'),
    });

    const formOptions = { resolver: yupResolver(validationSchema) };
    const { register, handleSubmit, reset, formState } = useForm(formOptions);
    const { errors } = formState;

    function onSubmit(data) {
        //console.log('onSubmit data: ' + data);
        actualizarDatos();
        eliminarDatos();
    }

    return (
        <>
            <form id='formulario' className='flex flex-col m-4 w-full h-full' onSubmit={handleSubmit(onSubmit)}>
                <div>
                    <Tippy
                        trigger='click'
                        placement={'left'}
                        animation='scale'
                        theme='light'
                        interactive={true}
                        content={
                            <Tippy>
                                <>
                                    <div
                                        className="flex flex-col justify-left items-left "
                                    >
                                        <button
                                            className="pt-1 pb-1 items-center flex"
                                            onClick={() => setShowEditModal(true)}
                                        >
                                            <svg
                                                xmlns="http://www.w3.org/2000/svg"
                                                fill="none"
                                                viewBox="0 0 24 24"
                                                strokeWidth="1.5"
                                                stroke="currentColor"
                                                className="w-5 h-5 text-[#643DCE]"
                                            >
                                                <path strokeLinecap="round" strokeLinejoin="round" d="M16.862 4.487l1.687-1.688a1.875 1.875 0 112.652 2.652L10.582 16.07a4.5 4.5 0 01-1.897 1.13L6 18l.8-2.685a4.5 4.5 0 011.13-1.897l8.932-8.931zm0 0L19.5 7.125M18 14v4.75A2.25 2.25 0 0115.75 21H5.25A2.25 2.25 0 013 18.75V8.25A2.25 2.25 0 015.25 6H10" />
                                            </svg>

                                            <p className="text-[14px] font-semibold hover:text-[#643DCE]">Editar</p>

                                        </button>

                                        <button
                                            className="pt-1 pb-1 items-center flex"
                                            onClick={() => setShowDeleteModal(true)}
                                        >
                                            <svg
                                                xmlns="http://www.w3.org/2000/svg"
                                                fill="none"
                                                viewBox="0 0 24 24"
                                                strokeWidth="1.5"
                                                stroke="currentColor"
                                                className="w-5 h-5 text-[#FF0000]"
                                            >
                                                <path strokeLinecap="round" strokeLinejoin="round" d="M14.74 9l-.346 9m-4.788 0L9.26 9m9.968-3.21c.342.052.682.107 1.022.166m-1.022-.165L18.16 19.673a2.25 2.25 0 01-2.244 2.077H8.084a2.25 2.25 0 01-2.244-2.077L4.772 5.79m14.456 0a48.108 48.108 0 00-3.478-.397m-12 .562c.34-.059.68-.114 1.022-.165m0 0a48.11 48.11 0 013.478-.397m7.5 0v-.916c0-1.18-.91-2.164-2.09-2.201a51.964 51.964 0 00-3.32 0c-1.18.037-2.09 1.022-2.09 2.201v.916m7.5 0a48.667 48.667 0 00-7.5 0" />
                                            </svg>
                                            <p className="text-[14px] font-semibold hover:text-[#FF0000]">
                                                Eliminar
                                            </p>
                                        </button>
                                    </div>
                                </>
                            </Tippy>
                        }>
                        <button>
                            <svg
                                width="8"
                                height="28"
                                viewBox="0 0 8 28"
                                fill="none"
                                xmlns="http://www.w3.org/2000/svg"
                                className='w-[10px] h-[23px] '
                            >
                                <path d="M4 9.93548C6.21111 9.93548 8 11.7532 8 14C8 16.2468 6.21111 18.0645 4 18.0645C1.78889 18.0645 0 16.2468 0 14C0 11.7532 1.78889 9.93548 4 9.93548ZM0 4.06452C0 6.31129 1.78889 8.12903 4 8.12903C6.21111 8.12903 8 6.31129 8 4.06452C8 1.81774 6.21111 0 4 0C1.78889 0 0 1.81774 0 4.06452ZM0 23.9355C0 26.1823 1.78889 28 4 28C6.21111 28 8 26.1823 8 23.9355C8 21.6887 6.21111 19.871 4 19.871C1.78889 19.871 0 21.6887 0 23.9355Z" fill="#D9D9D9" />
                            </svg>
                        </button>
                    </Tippy>
                </div>
                
                {showDeleteModal ? (
                    <>
                        <div className="fixed inset-0 z-10 overflow-y-auto">
                            <div
                                className="fixed inset-0 w-full h-full bg-[#000000] opacity-40"
                                onClick={() => setShowDeleteModal(false)}
                            />
                            <div className="flex items-center min-h-screen px-4 py-8">
                                <div className="relative w-full max-w-[600px] p-4 mx-auto bg-[#FFFF] rounded-md shadow-lg justify-center items-center flex flex-col ">
                                    <div
                                        className={'text-red hover:text-red'}
                                    >
                                        <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" strokeWidth="1.5" stroke="currentColor" className="w-16 h-16 text-[#FF0000]">
                                        <path strokeLinecap="round" strokeLinejoin="round" d="M9.75 9.75l4.5 4.5m0-4.5l-4.5 4.5M21 12a9 9 0 11-18 0 9 9 0 0118 0z" />
                                        </svg>


                                    </div>

                                    <div>
                                        <h1
                                            className="text-[32px]"
                                        >
                                            Are you sure?
                                        </h1>
                                    </div>

                                    <div
                                        className="flex flex-col justify-center items-center mt-[22px] "
                                    >
                                        <p
                                            className="text-[18px]  font-semibold "
                                        >
                                            Do you really want to delete these records?
                                        </p>
                                        <p
                                            className="text-[18px] font-light "
                                        >
                                            This process cannot be undone
                                        </p>
                                    </div>

                                    <div className="flex flex-row justify-between mt-[45px]">
                                        <div>
                                            <button
                                                className="w-[85px] h-[45px] border-[1px] border-[#3682F7] rounded-[20px] text-[#3682F7] hover:bg-[#3682F7] hover:text-[#FFFF] text-[14px] mt-[3px] mr-[10px]"
                                                onClick={() =>
                                                    setShowDeleteModal(false)
                                                }
                                            >
                                                Cancel
                                            </button>
                                            <button
                                                className="w-[85px] h-[45px] border-[1px] bg-[#FF0000] rounded-[20px] text-[#FFFF] hover:border-[#FF0000] hover:bg-[#FFFF] hover:text-[#FF0000] text-[14px] mt-[3px]"
                                                onClick={() => {
                                                    eliminarDatos(),
                                                    setShowDeleteModal(false)
                                                }}
                                            >
                                                Delete
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </>
                ) : null}

                {showEditModal ? (
                <>
                    <div className="fixed inset-0 z-10 overflow-y-auto">
                        <div
                            className="fixed inset-0 w-full h-full bg-[#000000] opacity-40"
                            onClick={() => setShowEditModal(false)}
                        ></div>
                        <div className="flex items-center min-h-screen px-4 py-8">
                            <div className="relative w-full max-w-[600px] p-4 mx-auto bg-[#FFFF] rounded-md shadow-lg">

                                <div>
                                    <h4 className="text-lg font-medium text-gray-800 text-start">
                                        EDIT PLANS
                                    </h4>

                                </div>

                                <div>
                                <form onSubmit={handleSubmit(onSubmit)}>

                                    <div className="mt-[20px] grid grid-cols-12 gap-4">

                                        <div className='col-span-12 md:col-span-12 lg:col-span-12 '>
            
                                                <p className='text-[12px] text-[#C1C1C1] mr-[10px] text-start'>Name</p>
                                                <input name="name" type="text" id="name" className={`w-full h-[30px] bg-[#FFFFFF] border-[1px] border-[#E4E7EB] rounded-[10px] pl-[10px] text-[12px]`}
                                                placeholder='Ingrese su nombre...'
                                                {...register('name')}
                                                value={nameForm}
                                                onChange={(e) => {
                                                    setNameForm(e.target.value)
                                                }}/>
                                                <div className="text-[14px] text-[#FF0000]">{errors.name?.message}</div>

                                        </div>

                                    
                                    </div>
                                

                                    <div className="mt-[20px] grid grid-cols-12 gap-3">

                                        <div className='col-span-6 md:col-span-6 lg:col-span-4 '>
                                            <div className="flex flex-row">
                                                <input name="facebook"   type="checkbox" id="" className="mr-[5px]" 
                                                {...register('facebook')}
                                                value={facebookForm}
                                                onChange={(e) => {
                                                    setFacebookForm(e.target.value)
                                                }}/>
                                                <Image
                                                src={ImgFacebook}
                                                alt='ImgFacebook'
                                                layout='fixed'
                                                width={25}
                                                height={25}
                                            />
                                            <input name="facebookPostsGty" type="number" {...register('facebookPostsGty')} className={`w-full h-[25px] bg-[#FFFFFF] border-[1px] border-[#E4E7EB] rounded-[10px] ml-[10px] pl-[5px] text-[12px]`}
                                            placeholder='Posts Qty'
                                            value={facebookPostsGtyForm}
                                            onChange={(e) => {
                                                setFacebookPostsGtyForm(e.target.value)
                                            }}
                                            />
                                            <div className="text-[14px] text-[#FF0000]">{errors.facebookPostsGty?.message}</div>
                                                
                                            </div>
                                        </div>

                                        <div className='col-span-6 md:col-span-6 lg:col-span-4 '>
                                            <div className="flex flex-row">
                                                <input name="instagram" type="checkbox" id="acceptTermsandConditions"  className="mr-[5px]" 
                                                {...register('instagram')}
                                                value={instagramForm}
                                                onChange={(e) => {
                                                    setInstagramForm(e.target.value)
                                                }}/>
                                                <Image
                                                src={ImgIg}
                                                alt='ImgIg'
                                                layout='fixed'
                                                width={25}
                                                height={25}
                                            />
                                            <input name="instagramPostsGty" type="number" {...register('instagramPostsGty')} className={`w-full h-[25px] bg-[#FFFFFF] border-[1px] border-[#E4E7EB] rounded-[10px] ml-[10px] pl-[5px] text-[12px]`}
                                            placeholder='Posts Qty'
                                            value={instagramPostsGtyForm}
                                            onChange={(e) => {
                                                setInstagramPostsGtyForm(e.target.value)
                                            }}
                                            />

                                            <div className="text-[14px] text-[#FF0000]">{errors.instagramPostsGty?.message}</div>
                                                
                                            </div>
                                        </div>

                                        <div className='col-span-6 md:col-span-6 lg:col-span-4 '>
                                            <div className="flex flex-row">
                                                <input name="mailing" type="checkbox" id="acceptTermsandConditions" className="mr-[5px]"
                                                {...register('mailing')}
                                                value={mailingForm}
                                                onChange={(e) => {
                                                    setMailingForm(e.target.value)
                                                }} />
                                                <Image
                                                src={ImgGmail}
                                                alt='ImgGmail'
                                                layout='fixed'
                                                width={25}
                                                height={25}
                                            />
                                            <input name="mailingPostsGty" type="number" {...register('mailingPostsGty')} className={`w-full h-[25px] bg-[#FFFFFF] border-[1px] border-[#E4E7EB] rounded-[10px] ml-[10px] pl-[5px] text-[12px]`}
                                            placeholder='Posts Qty'
                                            value={mailingPostsGtyForm}
                                            onChange={(e) => {
                                                setMailingPostsGtyForm(e.target.value)
                                            }}
                                            />
                                            <div className="text-[14px] text-[#FF0000]">{errors.mailingPostsGty?.message}</div>
                                                
                                            </div>
                                        </div>
                                        

                                        <div className='col-span-6 md:col-span-6 lg:col-span-3 '>
                                            <div className="flex flex-row">
                                                <input name="youtube" type="checkbox" id="acceptTermsandConditions" className="mr-[5px]"
                                                {...register('youtube')} 
                                                value={youtubeForm}
                                                onChange={(e) => {
                                                    setYoutubeForm(e.target.value)
                                                }}/>
                                                <Image
                                                src={ImgYoutube}
                                                alt='ImgYoutube'
                                                layout='fixed'
                                                width={25}
                                                height={25}
                                            />
                                            <input name="youtubePostsGty" type="number" {...register('youtubePostsGty')} className={`w-full h-[25px] bg-[#FFFFFF] border-[1px] border-[#E4E7EB] rounded-[10px] ml-[10px] pl-[5px] text-[12px]`}
                                            placeholder='Posts Qty'
                                            value={youtubePostsGtyForm}
                                            onChange={(e) => {
                                                setYoutubePostsGtyForm(e.target.value)
                                            }}
                                            />

                                            <div className="text-[14px] text-[#FF0000]">{errors.youtubePostsGty?.message}</div>
                                                
                                            </div>
                                        </div>

                                        <div className='col-span-6 md:col-span-6 lg:col-span-3 '>
                                            <div className="flex flex-row">
                                                <input name="tiktok" type="checkbox" id="acceptTermsandConditions" className="mr-[5px]"
                                                {...register('tiktok')}
                                                value={tiktokForm}
                                                onChange={(e) => {
                                                    setTiktokForm(e.target.value)
                                                }}/>
                                                <Image
                                                src={ImgTiktok}
                                                alt='ImgTiktok'
                                                layout='fixed'
                                                width={25}
                                                height={25}
                                            />
                                            <input name="tiktokPostsGty" type="number" {...register('tiktokPostsGty')} className={`w-full h-[25px] bg-[#FFFFFF] border-[1px] border-[#E4E7EB] rounded-[10px] ml-[10px] pl-[5px] text-[12px]`}
                                            placeholder='Posts Qty'
                                            value={tiktokPostsGtyForm}
                                            onChange={(e) => {
                                                setTiktokPostsGtyForm(e.target.value)
                                            }}
                                            />
                                            <div className="text-[14px] text-[#FF0000]">{errors.tiktokPostsGty?.message}</div>
                                                
                                            </div>
                                        </div>

                                        <div className='col-span-6 md:col-span-6 lg:col-span-3'>
                                            <div className="flex flex-row">
                                                <input name="linkedin" type="checkbox" id="acceptTermsandConditions" className="mr-[5px]"
                                                {...register('linkedin')}
                                                value={linkedinForm}
                                                onChange={(e) => {
                                                    setLinkedinForm(e.target.value)
                                                }}/>
                                                <Image
                                                src={ImgTelegram}
                                                alt='ImgTelegram'
                                                layout='fixed'
                                                width={25}
                                                height={25}
                                            />
                                            <input name="linkedinPostsGty" type="number" {...register('linkedinPostsGty')} className={`w-full h-[25px] bg-[#FFFFFF] border-[1px] border-[#E4E7EB] rounded-[10px] ml-[10px] pl-[5px] text-[12px]`}
                                            placeholder='Posts Qty'
                                            value={linkedinPostsGtyForm}
                                            onChange={(e) => {
                                                setLinkedinPostsGtyForm(e.target.value)
                                            }}
                                            />
                                            <div className="text-[14px] text-[#FF0000]">{errors.linkedinPostsGty?.message}</div>
                                                
                                            </div>
                                        </div>

                                        <div className='col-span-6 md:col-span-6 lg:col-span-3'>
                                            <div className="flex flex-row">
                                                <input name="twitter" type="checkbox" id="acceptTermsandConditions" {...register('twitter')} className="mr-[5px]"
                                                {...register('twitter')}
                                                value={twitterForm}
                                                onChange={(e) => {
                                                    setTwitterForm(e.target.value)
                                                }}/>
                                                <Image
                                                src={ImgTwitter}
                                                alt='ImgTwitter'
                                                layout='fixed'
                                                width={25}
                                                height={25}
                                            />
                                            <input name="twitterPostsGty" type="number" {...register('twitterPostsGty')} className={`w-full h-[25px] bg-[#FFFFFF] border-[1px] border-[#E4E7EB] rounded-[10px] ml-[10px] pl-[5px] text-[12px]`}
                                            placeholder='Posts Qty'
                                            value={twitterPostsGtyForm}
                                            onChange={(e) => {
                                                setTwitterPostsGtyForm(e.target.value)
                                            }}
                                            />
                                            <div className="text-[14px] text-[#FF0000]">{errors.twitterPostsGty?.message}</div>
                                                
                                            </div>
                                        </div>
                                        
                                    
                                    </div>

                                    <div className="mt-[15px] grid grid-cols-12 gap-3">

                                        <div className='col-span-6 md:col-span-6 lg:col-span-6 '>
                                            <div className="flex flex-row">
                                                <input name="acceptTermsandConditions" type="checkbox" id="acceptTermsandConditions"
                                                {...register('hasIncludedAds')}
                                                value={hasIncludedAdsForm}
                                                onChange={(e) => {
                                                    setHasIncludedAdsForm(e.target.value)
                                                }}/>
                                                <p className='text-[12px] text-[#C1C1C1] ml-[10px] '>Has included ads </p>
                                                
                                                
                                            </div>
                                            <input name="includedAdsQty" type="number" {...register('includedAdsQty')} className={`w-full h-[25px] bg-[#FFFFFF] border-[1px] border-[#E4E7EB] rounded-[10px] pl-[5px] text-[12px]`}
                                                placeholder='Posts Qty'
                                                value={includedAdsQtyForm}
                                                onChange={(e) => {
                                                    setIncludedAdsQtyForm(e.target.value)
                                                }}
                                                />
                                                <div className="text-[14px] text-[#FF0000]">{errors.includedAdsQty?.message}</div>
                                        </div>

                                        <div className='col-span-6 md:col-span-6 lg:col-span-6 '>
                                            <p className='text-[12px] text-[#C1C1C1] mr-[10px] text-start'>Price</p>
                                            <input name="price" type="text" {...register('price')} className={`w-full h-[25px] bg-[#FFFFFF] border-[1px] border-[#E4E7EB] rounded-[10px] pl-[10px] text-[12px]`}
                                            placeholder='Price'
                                            value={priceForm}
                                            onChange={(e) => {
                                                setPriceForm(e.target.value)
                                            }}
                                            />
                                            <div className="text-[14px] text-[#FF0000]">{errors.price?.message}</div>
                                                
                                        </div>
                                        

                                    </div>


                                    <div className="flex flex-row justify-between mt-[20px]">

                                        <div>
                                            <h1 className="text-[12px] mt-[10px]">* This field is mandatory</h1>
                                        </div>

                                        <div>
                                            <button
                                                className="w-[75px] h-[35px] border-[1px] border-[#582BE7] rounded-[30px] text-[#582BE7] hover:bg-[#582BE7] hover:text-[#FFFF] text-[14px] mt-[3px] mr-[10px]"
                                                onClick={() =>
                                                    setShowEditModal(false)
                                                }
                                            >
                                                Cancel
                                            </button>
                                            <button
                                                className="w-[100px] h-[35px] border-[1px] bg-[#582BE7] rounded-[30px] text-[#FFFFFF] hover:border-[#582BE7] hover:bg-[#FFFFFF] hover:b-[#FFFFFF]  hover:text-[#582BE7] text-[14px] mt-[3px]"
                                                type='submit'
                                                // onClick={() =>
                                                //     setShowModal(false)
                                                // }
                                            >
                                                Update
                                            </button>
                                        </div>
                                    </div>
                                </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </>
            ) : null}
            </form>
        </>
    )
}