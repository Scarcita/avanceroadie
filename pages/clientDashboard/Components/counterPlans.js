import React from 'react';
import Spinner from 'react-activity/dist/Spinner';
import 'react-activity/dist/Spinner.css';
import { useEffect, useState } from 'react'

const CounterPlan = (props) => {
    const [isLoading, setIsLoading] = useState(true);
    const [data, setData] = useState([]);
    const { client_id } = props;

    useEffect(() => {
        fetch('https://slogan.com.bo/roadie/clientsPlans/all/' + client_id)
            .then(response => response.json())
            .then(data => {
                if (data.status) {
                    console.warn('PRICEPLAN: ' + data.data);
                    setData(data.data)
                
                } else {
                    console.error(data.error)
                }
                setIsLoading(false)
            })


        }, [])

    return (
        isLoading ?
        <>
            <div className='flex justify-center items-center' style={{ width: '100%', height: 70 }}>
                <Spinner color="#582BE7" size={17} speed={1} animating={true} style={{ marginLeft: 'auto', marginRight: 'auto' }} />
            </div>

        </>
        :
        <div className='rounded-[22.35px] pt-6 h-full w-full overflow-auto'>
            <PlansTotal data={data}/>
        </div>
 
    )
};

const PlansTotal = (props) => {
    const { data } = props;
    //const map1 = data.map(row => console.log(row));
   


    return (
        <div>

            {data.map(row => 
            {

                 let total = parseFloat(row.plan.price) + parseFloat(row.plan.included_ads_qty)
                 console.log('suma: ' + total);
            
                return (
                    <div className="mt-[20px] grid grid-cols-12 gap-3"
                    key={row.id}
                        >
                        
                        <div className='col-span-6 md:col-span-6 lg:col-span-2 bg-[#F3F3F3] rounded-[16px] p-[10px]'>
                        {row.plan.price !== null ?
                            <div>
                                <p className="text-[24px] md:text-[28px] lg:text-[30px] font-semibold text-[#000000]">{row.plan.price} <span className="text-[16px] md:text-[20px] lg:text-[24px] font-semibold text-[#000000]">USD</span></p>
                                <p className="text-[11px] text-[#000000]">PLAN TOTAL</p>
                            </div>
                            : <></>
                        }
                        </div>
                        <div className='col-span-6 md:col-span-6 lg:col-span-2 bg-[#F3F3F3] rounded-[16px] p-[10px]'>
                        {row.plan.included_ads_qty !== null  ?
                            <div>
                                <p className="text-[24px] md:text-[28px] lg:text-[30px]  font-semibold text-[#000000]">{row.plan.included_ads_qty} <span className="text-[16px] md:text-[20px] lg:text-[24px] font-semibold text-[#000000]">USD</span> </p>
                                <p className="text-[11px] text-[#000000]">ADS TOTAL</p>
                            </div>
                            : <></>
                        }
                        </div>
                        <div className='col-span-6 md:col-span-6 lg:col-span-2 bg-[#643DCE] rounded-[16px] p-[10px]'>
                            <div>
                                <p className="text-[24px] md:text-[28px] lg:text-[30px]  font-semibold text-[#FFFFFF]">{total} <span className="text-[16px] md:text-[20px] lg:text-[24px] font-semibold text-[#FFFFFF]">USD</span> </p>
                                <p className="text-[11px] text-[#FFFFFF]">PLAN TOTAL</p>
                            </div>
                        </div>
                        <div className='col-span-6 md:col-span-6 lg:col-span-2 bg-[#F3F3F3] rounded-[16px] p-[10px]'>
                            <div>
                                <p className="text-[24px] md:text-[28px] lg:text-[30px]  font-semibold text-[#000000]">Yes</p>
                                <p className="text-[11px] text-[#000000]">Feb 2,2022 11:20 P.M.</p>
                                <p className="text-[11px] text-[#000000]">PAID</p>
                            </div>
                        </div>
                        <div className='col-span-6 md:col-span-6 lg:col-span-2 bg-[#F3F3F3] rounded-[16px] p-[10px]'>
                            <div>
                                <p className="text-[24px] md:text-[28px] lg:text-[30px] font-semibold text-[#000000]">Yes</p>
                                <p className="text-[11px] text-[#000000]">Feb 2,2022 11:20 P.M.</p>
                                <p className="text-[11px] text-[#000000]">INVOICED</p>
                            </div>
                        </div>
                        <div className='col-span-6 md:col-span-6 lg:col-span-2 bg-[#643DCE] rounded-[16px] p-[10px]'>
                            <div>
                                <p className="text-[24px] md:text-[28px] lg:text-[30px] font-semibold text-[#FFFFFF]"></p>
                                <p className="text-[11px] text-[#FFFFFF]">120.70 (Invoice)</p>
                                <p className="text-[11px] text-[#FFFFFF]">INVOICE TOTAL</p>
                            </div>
                        </div>
                    </div>

                )
                }
            )}
            
        </div>     
    );
};





export default CounterPlan;